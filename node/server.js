//importuri de pachete
const express = require("express");
const mysql = require("mysql");

//initializarea aplicatiei
const app = express();

//pentru ca aplicatia sa poata citi obiectele pe care le trimitem in request
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let port = 8080;

app.listen(port, () => {
  console.log("Serverul merge pe portul " + port);
});

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "training_crocos",
});

connection.connect((err) => {
  if (err) throw err;

  console.log("Baza de date conectat");

  const sqlQuery =
    "CREATE TABLE IF NOT EXISTS Crocodili(id INTEGER PRIMARY KEY  NOT NULL AUTO_INCREMENT, nume VARCHAR(30), prenume VARCHAR(30), telefon VARCHAR(10), email VARCHAR(30), activ BOOLEAN)";

  connection.query(sqlQuery, (err) => {
    if (err) throw err;
    else console.log("Tabela crocodili creata!");
  });
});

app.post("/crocodili", (req, res) => {
  const croco = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    email: req.body.email,
    telefon: req.body.telefon,
    activ: req.body.activ,
  };

  let errors = [];

  //validari
  if (
    !croco.nume ||
    !croco.prenume ||
    !croco.email ||
    !croco.telefon ||
    croco.activ === undefined
  ) {
    errors.push("Exista campuri necompletate!");
  }
  if (
    !croco.email.includes("@gmail.com") &&
    !croco.email.includes("@yahoo.com")
  ) {
    errors.push("Email incorect");
  }
  if (croco.telefon.length !== 10) {
    errors.push("Telefon incorect");
  }

  if (errors.length === 0) {
    try {
      const insertQuery = `INSERT INTO Crocodili(nume,prenume,email,telefon,activ)
         VALUES('${croco.nume}', '${croco.prenume}', '${croco.email}', '${croco.telefon}', '${croco.activ}')`;

      connection.query(insertQuery, (err) => {
        if (err) throw err;
        else {
          console.log("Bine ai venit, rau ai nimerit");
          res.status(201).send({ message: "Bine ai venit, rau ai nimerit" });
        }
      });
    } catch (err) {
      console.log("Server error");
      res.status(500).send(err);
    }
  } else {
    console.log("Eroare!");
    res.status(400).send(errors);
  }
});
app.get("/crocodili", (req, res) => {
  try {
    let select = "";
    if (req.query.activ) {
      select = `SELECT * FROM Crocodili WHERE activ = '${req.query.activ}'`;
    } else {
      select = "SELECT * FROM Crocodili";
    }
    connection.query(select, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
app.get("/crocodili/:id", (req, res) => {
  try {
    let select = "";
      select = `SELECT * FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(select, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});

app.delete("/crocodili/:id", (req, res) => {
  try {
    const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(sqlDelete, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil disparut" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
app.put("/crocodili/:id", (req, res) => {
  try {
    let update = "";
      update = `UPDATE Crocodili
      SET nume = '${req.body.nume}', prenume = '${req.body.prenume}', email = '${req.body.email}', telefon = '${req.body.telefon}', activ ='${req.body.activ}'
      WHERE id = '${req.params.id}'`;
    connection.query(update, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});

